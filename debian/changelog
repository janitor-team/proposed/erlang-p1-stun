erlang-p1-stun (1.0.37-1) unstable; urgency=medium

  * New upstream version 1.0.37
  * Updated Erlang dependencies

 -- Philipp Huebner <debalance@debian.org>  Sun, 02 Aug 2020 17:37:44 +0200

erlang-p1-stun (1.0.35-1) unstable; urgency=medium

  * New upstream version 1.0.35
  * Updated Erlang dependencies

 -- Philipp Huebner <debalance@debian.org>  Sun, 26 Jul 2020 21:50:11 +0200

erlang-p1-stun (1.0.32-1) unstable; urgency=medium

  * New upstream version 1.0.32
  * Updated Erlang dependencies

 -- Philipp Huebner <debalance@debian.org>  Fri, 01 May 2020 12:31:51 +0200

erlang-p1-stun (1.0.31-1) unstable; urgency=medium

  * New upstream version 1.0.31
  * Updated Erlang dependencies

 -- Philipp Huebner <debalance@debian.org>  Wed, 18 Mar 2020 12:02:27 +0100

erlang-p1-stun (1.0.30-1) unstable; urgency=medium

  * New upstream version 1.0.30
  * Fixed typo in long package description.
  * Updated Standards-Version: 4.5.0 (no changes needed)
  * Updated Erlang dependencies
  * Updated years in debian/copyright
  * Rules-Requires-Root: no

 -- Philipp Huebner <debalance@debian.org>  Wed, 05 Feb 2020 19:33:36 +0100

erlang-p1-stun (1.0.29-1) unstable; urgency=medium

  * New upstream version 1.0.29
  * Updated Erlang dependencies

 -- Philipp Huebner <debalance@debian.org>  Sat, 10 Aug 2019 14:22:39 +0200

erlang-p1-stun (1.0.28-1) unstable; urgency=medium

  * New upstream version 1.0.28
  * Updated Erlang dependencies
  * Updated Standards-Version: 4.4.0 (no changes needed)
  * debian/rules: export ERL_COMPILER_OPTIONS=deterministic
  * Updated debhelper compat version: 12

 -- Philipp Huebner <debalance@debian.org>  Wed, 24 Jul 2019 12:50:05 +0200

erlang-p1-stun (1.0.26-1) unstable; urgency=medium

  * New upstream version 1.0.26
  * Updated Standards-Version: 4.3.0 (no changes needed)
  * Updated Erlang dependencies
  * Updated years in debian/copyright

 -- Philipp Huebner <debalance@debian.org>  Tue, 01 Jan 2019 22:48:59 +0100

erlang-p1-stun (1.0.25-1) unstable; urgency=medium

  * New upstream version 1.0.25
  * Enabled DH_VERBOSE in debian/rules
  * Updated Standards-Version: 4.2.1 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Sun, 07 Oct 2018 14:57:48 +0200

erlang-p1-stun (1.0.23-1) unstable; urgency=medium

  * New upstream version 1.0.23
  * Added debian/upstream/metadata

 -- Philipp Huebner <debalance@debian.org>  Wed, 04 Jul 2018 16:24:24 +0200

erlang-p1-stun (1.0.22-1) unstable; urgency=medium

  * New upstream version 1.0.22
  * Updated Standards-Version: 4.1.4 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Wed, 09 May 2018 17:20:22 +0200

erlang-p1-stun (1.0.21-1) unstable; urgency=medium

  * use secure copyright format uri in debian/copyright
  * New upstream version 1.0.21
  * Switched to debhelper 11

 -- Philipp Huebner <debalance@debian.org>  Tue, 27 Mar 2018 23:30:10 +0200

erlang-p1-stun (1.0.20-1) unstable; urgency=medium

  * New upstream version 1.0.20
  * (Build-)Depend on erlang-p1-tls (>= 1.0.20)

 -- Philipp Huebner <debalance@debian.org>  Sat, 13 Jan 2018 12:48:45 +0100

erlang-p1-stun (1.0.17-1) unstable; urgency=medium

  * Set Maintainer: Ejabberd Packaging Team <ejabberd@packages.debian.org>
  * Set Uploaders: Philipp Huebner <debalance@debian.org>
  * Updated Vcs-* fields in debian/control for salsa.debian.org
  * Updated years in debian/copyright
  * Updated Standards-Version: 4.1.3 (no changes needed)
  * New upstream version 1.0.17
  * (Build-)Depend on erlang-p1-tls (>= 1.0.18)

 -- Philipp Huebner <debalance@debian.org>  Thu, 04 Jan 2018 11:37:58 +0100

erlang-p1-stun (1.0.16-1) unstable; urgency=medium

  * New upstream version 1.0.16
  * (Build-)Depend on erlang-base (>= 1:19.2)
  * Updated Standards-Version: 4.1.1 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Mon, 27 Nov 2017 20:21:41 +0100

erlang-p1-stun (1.0.14-1) unstable; urgency=medium

  * New upstream version 1.0.14
  * Updated Standards-Version: 4.1.0 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Sat, 02 Sep 2017 17:28:48 +0200

erlang-p1-stun (1.0.12-1) unstable; urgency=medium

  * New upstream version 1.0.12
  * Updated Standards-Version: 4.0.0 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Wed, 12 Jul 2017 18:34:06 +0200

erlang-p1-stun (1.0.10-1~exp1) experimental; urgency=medium

  * New upstream version 1.0.10
  * Updated debian/copyright

 -- Philipp Huebner <debalance@debian.org>  Sat, 15 Apr 2017 13:24:41 +0200

erlang-p1-stun (1.0.7-2) unstable; urgency=medium

  * Added erlang-base to Build-Depends

 -- Philipp Huebner <debalance@debian.org>  Wed, 05 Oct 2016 15:57:52 +0200

erlang-p1-stun (1.0.7-1) unstable; urgency=medium

  * New upstream version 1.0.7
  * (Build-)Depend on erlang-p1-utils (>= 1.0.5) and erlang-p1-tls (>= 1.0.7)

 -- Philipp Huebner <debalance@debian.org>  Fri, 16 Sep 2016 14:36:06 +0200

erlang-p1-stun (1.0.6-1) unstable; urgency=medium

  * Imported Upstream version 1.0.6
  * (Build-)Depend on erlang-p1-tls (>= 1.0.6)

 -- Philipp Huebner <debalance@debian.org>  Fri, 05 Aug 2016 21:15:28 +0200

erlang-p1-stun (1.0.5-1) unstable; urgency=medium

  * Imported Upstream version 1.0.5

 -- Philipp Huebner <debalance@debian.org>  Sun, 03 Jul 2016 16:42:40 +0200

erlang-p1-stun (1.0.4-1) unstable; urgency=medium

  * Imported Upstream version 1.0.4
  * (Build-)Depend on erlang-p1-tls (>= 1.0.4)
  * Improved debian/watch
  * Updated Standards-Version: 3.9.8 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Sun, 26 Jun 2016 16:10:27 +0200

erlang-p1-stun (1.0.1-1) unstable; urgency=medium

  * Updated debian/copyright
  * Moved back to erlang-p1-tls

 -- Philipp Huebner <debalance@debian.org>  Sun, 14 Feb 2016 18:26:43 +0100

erlang-p1-stun (1.0.1-1~exp1) experimental; urgency=medium

  * Imported Upstream version 1.0.1

 -- Philipp Huebner <debalance@debian.org>  Fri, 05 Feb 2016 19:00:14 +0100

erlang-p1-stun (1.0.0-1~exp1) experimental; urgency=medium

  * Imported Upstream version 1.0.0
  * Moved to erlang-p1-fast-tls

 -- Philipp Huebner <debalance@debian.org>  Thu, 04 Feb 2016 15:55:13 +0100

erlang-p1-stun (0.2016.01.23-1) unstable; urgency=medium

  * Imported Upstream version 0.2016.01.23 (aka 0.9.1)
  * Updated Standards-Version: 3.9.7 (no changes needed)
  * Updated Vcs-* fields in debian/control

 -- Philipp Huebner <debalance@debian.org>  Wed, 03 Feb 2016 11:33:01 +0100

erlang-p1-stun (0.2016.01.05-1) unstable; urgency=medium

  * Imported Upstream version 0.2016.01.05 (aka 0.9.0)
  * Updated debian/copyright
  * Updated debian/watch

 -- Philipp Huebner <debalance@debian.org>  Sat, 16 Jan 2016 13:00:11 +0100

erlang-p1-stun (0.2015.09.16-1) unstable; urgency=medium

  * Imported Upstream version 0.2015.09.16

 -- Philipp Huebner <debalance@debian.org>  Sun, 04 Oct 2015 10:23:22 +0200

erlang-p1-stun (0.2015.07.21-1) unstable; urgency=medium

  * Imported Upstream version 0.2015.07.21
  * Enabled eunit

 -- Philipp Huebner <debalance@debian.org>  Mon, 17 Aug 2015 16:58:18 +0200

erlang-p1-stun (0.2014.08.20-3) unstable; urgency=high

  [ Holger Weiss ]
  * Support Erlang/OTP 18.x (Closes: #790445)

 -- Philipp Huebner <debalance@debian.org>  Sat, 04 Jul 2015 14:24:14 +0200

erlang-p1-stun (0.2014.08.20-2) unstable; urgency=medium

  * Updated Standards-Version: 3.9.6 (no changes needed)
  * Updated years in debian/copyright
  * Added Vcs links to debian/control

 -- Philipp Huebner <debalance@debian.org>  Sat, 02 May 2015 18:28:22 +0200

erlang-p1-stun (0.2014.08.20-1) unstable; urgency=medium

  * Imported Upstream version 0.2014.08.20

 -- Philipp Huebner <debalance@debian.org>  Thu, 28 Aug 2014 12:14:46 +0200

erlang-p1-stun (0.2014.05.08-1) unstable; urgency=medium

  * Imported Upstream version 0.2014.05.08
  * Added new build-dependency: erlang-eunit

 -- Philipp Huebner <debalance@debian.org>  Wed, 14 May 2014 10:56:26 +0200

erlang-p1-stun (0.2013.06.23-1) unstable; urgency=low

  * Initial release, see #744885 for background

 -- Philipp Huebner <debalance@debian.org>  Wed, 23 Apr 2014 09:56:59 +0200
